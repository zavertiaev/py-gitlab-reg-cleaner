import re
import os
import gitlab

REP_URL = os.environ["REP_URL"]
PROJECT_NAME = os.environ["PROJECT_NAME"]
TOKEN = os.environ["TOKEN"]
SEARCH_FIELD = os.environ["SEARCH_FIELD"]
REGEX = os.environ["REGEX"]
TYPE = os.environ["TYPE"]


def search(field):
    x = re.search(REGEX, field)
    if (x and TYPE == 'include') or (x == None and TYPE == 'exclude'):
        return True
    else:
        return False


def delete(package):
    # package.delete()
    print(f"DELETED {package.name} {package.version}")


gl = gitlab.Gitlab(url=REP_URL, private_token=TOKEN)
# gl.auth()
# gl.enable_debug()
project = gl.projects.get(PROJECT_NAME)
packages = project.packages.list(get_all=True)
for package in packages:
    if SEARCH_FIELD == 'name' or SEARCH_FIELD == 'version':
        if search(getattr(package, SEARCH_FIELD)):
            delete(package)
    elif SEARCH_FIELD == 'tags':
        for tag in getattr(package, SEARCH_FIELD):
            if (search(tag["name"])):
                delete(package)
